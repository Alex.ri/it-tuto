import { Component } from '@angular/core';
import { ApplicationStateService } from './services/application-state/application-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isCollapsed = false;
  title = 'MyDevCommunity';
  isMobile: boolean;
  start: boolean = true;
  openMap: { [name: string]: boolean } = {
    sub1: false,
    sub2: false,
    sub3: false,
    sub4: false,
    sub5: false,
    sub6: false,
    sub7: false,
    sub8: false,
  };

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.start = false;
    }, 4000);
  }

  ngAfterViewChecked(): void {
    setTimeout(() => {
      this.start = false;
    }, 4000);
  }

  constructor(
    private appstate: ApplicationStateService
  ) {
  }

  openHandler(value: string): void {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }

  toggleCollapsed(): void {
    this.isMobile = this.appstate.getIsMobileResolution();
      if(this.isMobile) {
        this.isCollapsed = !this.isCollapsed;
      }
  }

  stateNav(): void {
    let home = document.querySelector('.body-content.ant-layout');
    let home2 = document.querySelector('.body2.ant-layout');
    let logo = document.querySelector('.display-logo');
    let isMobile = this.appstate.getIsMobileResolution();  
    
    /** open sidenav on mobile */
    if(this.isCollapsed == false && isMobile) {
      home2.classList.remove('hidden');
      logo.classList.remove('hidden');
      home.classList.add('hidden');
      home2.classList.add('second-body')
      logo.classList.add('vertical-logo');
    } else {
      /** close sidenav */
      home2.classList.add('hidden');
      logo.classList.add('hidden');
      home.classList.remove('hidden');
      home2.classList.remove('second-body');
      logo.classList.remove('vertical-logo');
    }
  }

}
