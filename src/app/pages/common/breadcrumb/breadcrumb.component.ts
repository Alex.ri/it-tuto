import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  currentUrl: string;

  constructor(private router: Router) {
    this.currentUrl = (this.router.url.replace('/', '')).replace('-', ' ').replace('techno/','');
    this.currentUrl = this.currentUrl[0].toUpperCase() + this.currentUrl.slice(1);
  }

  ngOnInit(): void {
  }

}
