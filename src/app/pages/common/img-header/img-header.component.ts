import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import typer from 'typer-js';
import { isArray } from 'util';

@Component({
  selector: 'app-img-header',
  templateUrl: './img-header.component.html',
  styleUrls: ['./img-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ImgHeaderComponent implements OnInit {
  hiddenLogo: Boolean = true;
  menu = ['home', 'login', 'forum', 'tutorial', 'register', 'forgot-password', 'add-question', 'forum-subject', 'tuto-subject'];

  description: string = "Une question, besoin de réponse, besoin d'aide... vous êtes au bon endroit, vous trouverez une/des solutions à vos problèmes." +
    " Notre mission: aider les gens à apprendre à coder gratuitement." +
    " Nous accomplissons cela en créant des milliers d'articles et de leçons de codage interactives, toutes accessibles gratuitement au public.";
  currentUrl: string;
  allImg = [
    { name: "Angular", url: '<img class="language-logo flip-scale-up-hor" src="https://i.ibb.co/HCscRLk/angular.png">' },
    { name: "Vue js", url: '<img class="language-logo flip-scale-up-hor" src="https://i.ibb.co/TBK2wfb/vue-js.png">' },
    { name: "React", url: '<img class="language-logo flip-scale-up-hor" src="https://i.ibb.co/jGC1bZY/react.png">' },
    { name: "Js", url: '<img class="language-logo" src="https://i.ibb.co/9H5F9jL/javascript.png">' },
    { name: "Jquery", url: '<img class="language-logo" src="https://i.ibb.co/gJMwFRb/jquery.png">' },

    { name: "Symfony", url: '<img class="language-logo" src="https://i.ibb.co/VHH4bm0/symfony.png">' },
    { name: "Php", url: '<img class="language-logo" src="https://i.ibb.co/Z6g0dnx/php-1.png">' },
    { name: "Laravel", url: '<img class="language-logo" src="https://i.ibb.co/BCYm1pJ/laravel.png">' },

    { name: "Prestashop", url: '<img class="language-logo" src="https://i.ibb.co/fnTz8YG/prestashop.png">' },
    { name: "Wordpress", url: '<img class="language-logo" src="https://i.ibb.co/k22TgBF/wordpress-blue.png">' },
    { name: "Joomla", url: '<img class="language-logo" src="https://i.ibb.co/gVzcy4p/joomla.png">' },
    { name: "Drupal", url: '<img class="language-logo" src="https://i.ibb.co/C1YmKyG/drupal.png">' },
    { name: "Magento", url: '<img class="language-logo" src="https://i.ibb.co/kxPw94P/magento-1.png">' },

    { name: "Markdown", url: '<img class="language-logo" src="https://i.ibb.co/q0Kd4VZ/markdown.png">' },
    { name: "Asciidoc", url: '<img class="language-logo" src="https://i.ibb.co/mDd0p8x/ascii-1.png">' },

    { name: "Css", url: '<img class="language-logo" src="https://i.ibb.co/Xpr08D8/css-5.png">' },
    { name: "Sass", url: '<img class="language-logo" src="https://i.ibb.co/1ZP0vbw/sass-1.png">' },

    { name: "Ionic", url: '<img class="language-logo" src="https://i.ibb.co/kJM8B7Q/ionic.png">' },
    { name: "Flutter", url: '<img class="language-logo" src="https://i.ibb.co/Mcrs29Q/flutter.png">' },
    { name: "Vue native", url: '<img class="language-logo" src="https://i.ibb.co/TBK2wfb/vue-js.png">' },
    { name: "React native", url: '<img class="language-logo" src="https://i.ibb.co/jGC1bZY/react.png">' },

    { name: "Mysql", url: '<img class="language-logo" src="https://i.ibb.co/jLwmrGK/mysql.png">' },
    { name: "Mongodb", url: '<img class="language-logo" src="https://i.ibb.co/nPwsjMC/mongodb.png">' },

    { name: "Python", url: '<img class="language-logo" src="https://i.ibb.co/60wSb3f/python-5.png">' },
  ];

  languageImg = '';


  constructor(public router: Router) {
    this.currentUrl = this.router.url.replace('/', '');
    this.currentUrl = this.currentUrl.replace('techno/', '');
    this.currentUrl != "home" ? this.description = "" : '';
  }

  ngOnInit() {
    setTimeout(() => {
      this.checkUrlPage();
    }, 500);

  }

  checkMenu(menu: string) {
    let check: Boolean = true;
    this.menu.includes(menu) ? check = true : check = false;
    return check;
  }

  checkUrlPage() {

    switch (this.currentUrl) {
      case "home":
        this.hiddenLogo = false;
        // this.animateTitleStep("&nbsp;Tutorial", " Forum", " Actualité", "MyDevCommunity");
        break;
      case "forum":
        this.setAnimateTitle("Forum");
        break;
      case "tutorial":
        this.setAnimateTitle("Tutorial");
        break;
      case "login":
        this.setAnimateTitle("Connexion");
        break;
      case "register":
        this.setAnimateTitle("Inscription");
        break;
      case "forgot-password":
        this.setAnimateTitle("Mot de passe oublier");
        break;
      case "add-question":
        this.setAnimateTitle("Poser une question publique");
        break;
      case "forum-subject":
        this.setAnimateTitle("Questions");
        break;
      case "tuto-subject":
        this.setAnimateTitle("Tutorial");
        break;
      default:
        this.currentUrl = this.currentUrl.replace('-', ' ');
        this.setAnimateTitle(this.currentUrl[0].toUpperCase() + this.currentUrl.slice(1));
        this.setImg(this.currentUrl[0].toUpperCase() + this.currentUrl.slice(1));
        break;
    }
  }


  setImg(name: string) {
    const t = this.allImg.filter((i) => {
      return i.name == name
    });
    t.length > 0 ? this.languageImg = t[0].url : '';
  }

  setAnimateTitle(text: string): void {
    typer('.title-chalk')
      .line(text);
  }

  animateTitleStep(p1: string, p2: string, p3: string, p4: string): void {
    typer('.title-chalk')
      .line(p1)
      .pause(1000)
      .back(8)
      .continue(p2)
      .pause(1000)
      .back(5)
      .continue(p3)
      .pause(1000)
      .back(9)
      .continue(p4);
  }

}
