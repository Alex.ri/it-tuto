import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-language-home',
  templateUrl: './language-home.component.html',
  styleUrls: ['./language-home.component.scss'],
})
export class LanguageHomeComponent implements OnInit {

  language: string;

  constructor(
    private router: Router,
    public myElement: ElementRef
    ) {
    this.language = this.router.url.replace('/','');
  }

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }
  
  ngAfterContentInit(): void {
    setTimeout(() => {
      this.scroll();  
    }, 500); 
  }

  scroll() {
    let el = this.myElement.nativeElement.querySelector('#top-header');
    el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }

}
