import { Component, OnInit } from '@angular/core';

interface ItemData {
  title: string;
  avatar: string;
  tag: string;
  content: string;
  author: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  data: ItemData[] = [];
  
  constructor() { }

  ngOnInit() {
    this.loadData(1);
  }

  ngAfterViewInit(): void {
    this.animateCard();
  }

  loadData(pi: number): void {
    this.data = new Array(3).fill({}).map((_, index) => {
      return {
        title: `Problème liée à l'integration OVH part ${index} (page: ${pi})`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        tag: 'JavaScript | Angular | Front-End',
        content:
          'We supply a series of design principles, practical patterns and high quality design resources ' +
          '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
        author: 'Auteur : RIGUEUR Alex'
      };
    });
  }

  animateCard(): void {
    $('.post-module').hover(function () {
      $(this).find('.description').stop().animate({
        height: "toggle",
        opacity: "toggle"
      }, 300);
    });
  }

}
