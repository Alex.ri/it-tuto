import { Component, OnInit, ElementRef } from '@angular/core';
import { ToastMessageService } from 'src/app/services/function/toast-message/toast-message.service';
import { distanceInWords } from 'date-fns';

@Component({
  selector: 'app-details-tuto',
  templateUrl: './details-tuto.component.html',
  styleUrls: ['./details-tuto.component.scss']
})
export class DetailsTutoComponent implements OnInit {
  // display comment variable
  likes = 0;
  dislikes = 0;
  time = distanceInWords(new Date(), new Date());

  // reply variable
  data: any[] = [];
  submitting = false;
  user = {
    author: 'Han Solo',
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
  };
  inputValue = '';

  constructor(
    private toast: ToastMessageService,
    public myElement: ElementRef
  ) { }

  ngOnInit(): void {
  }
  
  ngAfterContentInit(): void {
    setTimeout(() => {
      this.scroll();  
    }, 500); 
  }

  scroll() {
    let el = this.myElement.nativeElement.querySelector('#top-header');
    el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }

  like(): void {
    this.likes = 1;
    this.dislikes = 0;
  }

  dislike(): void {
    this.likes = 0;
    this.dislikes = 1;
  }

  handleSubmit(): void {
    this.submitting = true;
    const content = this.inputValue;
    this.inputValue = '';
    setTimeout(() => {
      this.submitting = false;
      this.data = [
        ...this.data,
        {
          ...this.user,
          content,
          datetime: new Date(),
          displayTime: distanceInWords(new Date(), new Date())
        }
      ].map(e => {
        return {
          ...e,
          displayTime: distanceInWords(new Date(), e.datetime)
        };
      });
    }, 800);
    this.toast.success("Commentaire ajouté avec succès.","")
  }

}
