import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsTutoComponent } from './details-tuto.component';

describe('DetailsTutoComponent', () => {
  let component: DetailsTutoComponent;
  let fixture: ComponentFixture<DetailsTutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsTutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsTutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
