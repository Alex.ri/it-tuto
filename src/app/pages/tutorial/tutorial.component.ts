import { Component, OnInit } from '@angular/core';

interface ItemData {
  href: string;
  title: string;
  avatar: string;
  tag: string;
  content: string;
  author: string;
}

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {
  data: ItemData[] = [];
  listOfData = [
    {
      name: 'Unable to define custom headers using HttpHeaders on Angular 8',
      date: "24/03/2020",
      description: 'I am trying to run a GET request to SAP Business Objects which requires some custom headers. I followed Angular documentation to define the headers using HttpHeaders class but it seems like the custom ...'
    },
    {
      name: 'After opening the keyboard in ios 12 (ionic 3), user-select: auto stops working',
      date: "22/12/2019",
      description: 'I am trying to make text selectable for copy and paste in my ionic app using user-select: text. This does work fine until the keyboard is opened. After that I can no longer select the text that I ...'
    }
  ];

  ngOnInit() {
    this.loadData(1);
  }

  loadData(pi: number): void {
    this.data = new Array(3).fill({}).map((_, index) => {
      return {
        href: 'http://ant.design',
        title: `Problème liée à l'integration OVH part ${index} (page: ${pi})`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        tag: 'JavaScript | Angular | Front-End',
        content:
          'We supply a series of design principles, practical patterns and high quality design resources ' +
          '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
        author: 'Auteur : RIGUEUR Alex'
      };
    });
  }

}
