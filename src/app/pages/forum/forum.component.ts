import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ForumComponent implements OnInit {

  dataSet = [
    {
      key: '1',
      titre: 'SSRS Report ambiguos behavior Page size Not showing Correctly',
      age: 32,
      last: 'GHEZAL Ilies',
      content: "I am trying to access a file in Processing Android Mode using the following: File file"+
       "= new File(filePath); And I tried the following options for setting filePath for the file “1.sf2” in the “data” ...',"+
      "address: 'New York No. 1 Lake Park'"
    },
    {
      key: '2',
      titre: 'Get Error 530 5.7.0 Authentication Required when sending email using Django Python',
      age: 42,
      last: 'GHEZAL Ilies',
      content: "I am trying to access a file in Processing Android Mode using the following: File file"+
       "= new File(filePath); And I tried the following options for setting filePath for the file “1.sf2” in the “data” ...',"+
      "address: 'New York No. 1 Lake Park'"
    },
    {
      key: '3',
      titre: 'How to access a file in Processing Android Mode',
      age: 32,
      last: 'GHEZAL Ilies',
      content: "I am trying to access a file in Processing Android Mode using the following: File file"+
       "= new File(filePath); And I tried the following options for setting filePath for the file “1.sf2” in the “data” ...',"+
      "address: 'New York No. 1 Lake Park'"
    }
  ];

  ngOnInit() { }

}
