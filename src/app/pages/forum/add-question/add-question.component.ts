import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UploadFile } from 'ng-zorro-antd';
import { ToastMessageService } from 'src/app/services/function/toast-message/toast-message.service';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {
  // FormControl variable
  validateForm: FormGroup;
  isLoadingTwo = false
  // Upload variable
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [
    {
      uid: -1,
      name: 'xxx.png',
      status: 'done',
      url: 'https://code.visualstudio.com/assets/docs/nodejs/angular/peek-definition.png'
    }
  ];
  previewImage: string | undefined = '';
  previewVisible = false;

  constructor(
    private fb: FormBuilder,
    private toast: ToastMessageService,
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      titreForum: [null, [Validators.required]],
      content: [null, [Validators.required]],
      listOfTagOptions: [[], [Validators.required]]
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  loadTwo(): void {
    this.isLoadingTwo = true;
    setTimeout(() => {
      this.isLoadingTwo = false;
      this.toast.success("Information", "La question à bien été soumis, elle sera visible immédiatement au publique."+
       "Remarque : Vous pouvez modifier le contenue de votre post dans votre profil.")
    }, 5000);
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };

}
