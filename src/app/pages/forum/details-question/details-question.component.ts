import { Component, OnInit, ElementRef } from '@angular/core';
import { distanceInWords } from 'date-fns';
import { ToastMessageService } from 'src/app/services/function/toast-message/toast-message.service';

@Component({
  selector: 'app-details-question',
  templateUrl: './details-question.component.html',
  styleUrls: ['./details-question.component.scss']
})
export class DetailsQuestionComponent implements OnInit {
  // display comment variable
  likes = 0;
  dislikes = 0;
  time = distanceInWords(new Date(), new Date());

  // reply variable
  data: any[] = [];
  submitting = false;
  user = {
    author: 'Han Solo',
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
  };
  inputValue = '';


  constructor(
    private toast: ToastMessageService,
    public myElement: ElementRef
  ) { }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.scroll();  
    }, 500); 
  }

  ngOnInit(): void {
  }

  like(): void {
    this.likes = 1;
    this.dislikes = 0;
  }

  dislike(): void {
    this.likes = 0;
    this.dislikes = 1;
  }

  handleSubmit(): void {
    this.submitting = true;
    const content = this.inputValue;
    this.inputValue = '';
    setTimeout(() => {
      this.submitting = false;
      this.data = [
        ...this.data,
        {
          ...this.user,
          content,
          datetime: new Date(),
          displayTime: distanceInWords(new Date(), new Date())
        }
      ].map(e => {
        return {
          ...e,
          displayTime: distanceInWords(new Date(), e.datetime)
        };
      });
    }, 800);
    this.toast.success("Commentaire ajouté avec succès.","")
  }

  scroll() {
    let el = this.myElement.nativeElement.querySelector('#top-header');
    el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }

}
