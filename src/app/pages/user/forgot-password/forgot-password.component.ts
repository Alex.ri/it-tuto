import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastMessageService } from 'src/app/services/function/toast-message/toast-message.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  validateForm: FormGroup;
  isLoadingTwo = false;

  constructor(
    private fb: FormBuilder,
    private toast: ToastMessageService 
    ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      emailUser: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  loadTwo(): void {
    this.isLoadingTwo = true;
    setTimeout(() => {
      this.isLoadingTwo = false;
      this.toast.info("Réinitialisation Mot de passe", "La réinitialisation de votre mot de passe c'est effectuer avec succées."+
      "<br> Vous recevrez un mail dans votre boite dans quelque minutes avec la procédure à suivre.")
    }, 5000);
  }

}
