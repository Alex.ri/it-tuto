import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ForumComponent } from './pages/forum/forum.component';
import { TutorialComponent } from './pages/tutorial/tutorial.component';
import { LoginComponent } from './pages/login/login.component';
import { LanguageHomeComponent } from './pages/language/language-home/language-home.component';
import { RegisterComponent } from './pages/register/register.component';
import { ForgotPasswordComponent } from './pages/user/forgot-password/forgot-password.component';
import { AddQuestionComponent } from './pages/forum/add-question/add-question.component';
import { NotFoundComponent } from './pages/common/not-found/not-found.component';
import { DetailsQuestionComponent } from './pages/forum/details-question/details-question.component';
import { DetailsTutoComponent } from './pages/tutorial/details-tuto/details-tuto.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomeComponent },
  { path: 'forum', component: ForumComponent },
  { path: 'tutorial', component: TutorialComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'add-question', component: AddQuestionComponent },
  { path: 'techno/:language', component: LanguageHomeComponent },
  { path: 'forum-subject', component: DetailsQuestionComponent },
  { path: 'tuto-subject', component: DetailsTutoComponent },

  // redirect 404
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: '/404'}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }