import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NgZorroAntdModule, NZ_I18N, fr_FR } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import fr from '@angular/common/locales/fr';
import { HomeComponent } from './pages/home/home.component';
import { ForumComponent } from './pages/forum/forum.component';
import { TutorialComponent } from './pages/tutorial/tutorial.component';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import typer from 'typer-js';
import { NavbarComponent } from './pages/common/navbar/navbar.component';
import { FooterComponent } from './pages/common/footer/footer.component';
import { ImgHeaderComponent } from './pages/common/img-header/img-header.component';
import { LanguageHomeComponent } from './pages/language/language-home/language-home.component';
import { RegisterComponent } from './pages/register/register.component';
import { LanguageDetailsComponent } from './pages/language/language-details/language-details.component';
import { ProfileComponent } from './pages/user/profile/profile.component';
import { ForgotPasswordComponent } from './pages/user/forgot-password/forgot-password.component';
import { AddQuestionComponent } from './pages/forum/add-question/add-question.component';
import { BreadcrumbComponent } from './pages/common/breadcrumb/breadcrumb.component';
import { NotFoundComponent } from './pages/common/not-found/not-found.component';
import { DetailsQuestionComponent } from './pages/forum/details-question/details-question.component';
import { DetailsTutoComponent } from './pages/tutorial/details-tuto/details-tuto.component';
import { SplashScreenComponent } from './pages/common/splash-screen/splash-screen.component';


registerLocaleData(fr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ForumComponent,
    TutorialComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    ImgHeaderComponent,
    LanguageHomeComponent,
    RegisterComponent,
    LanguageDetailsComponent,
    ProfileComponent,
    ForgotPasswordComponent,
    AddQuestionComponent,
    BreadcrumbComponent,
    NotFoundComponent,
    DetailsQuestionComponent,
    DetailsTutoComponent,
    SplashScreenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: fr_FR }],
  bootstrap: [AppComponent]
})
export class AppModule { }
