import { Injectable } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class ToastMessageService {

  constructor(private modalService: NzModalService) { }

  info(title: string, content:string): void {
    const modal = this.modalService.info({
      nzStyle: { top: '25%'},
      nzTitle: title,
      nzContent: content
    });
    setTimeout(() => modal.destroy(), 2500);
  }

  success(title: string, content:string): void {
    const modal = this.modalService.success({
      nzStyle: { top: '25%'},
      nzTitle: title,
      nzContent: content
    });
    setTimeout(() => modal.destroy(), 2500);
  }

  error(title: string, content:string): void {
    const modal = this.modalService.error({
      nzStyle: { top: '25%'},
      nzTitle: title,
      nzContent: content
    });
    setTimeout(() => modal.destroy(), 2500);
  }

  warning(title: string, content:string): void {
    const modal = this.modalService.warning({
      nzStyle: { top: '25%'},
      nzTitle: title,
      nzContent: content
    });
    setTimeout(() => modal.destroy(), 2500);
  }


}
