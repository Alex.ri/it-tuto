import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApplicationStateService {

  private isMobileResolution: boolean;

  constructor() {
    
  }

  setIsMobileResolution() {
    this.isMobileResolution = false;
    if (window.innerWidth < 768) {
      this.isMobileResolution = true;
    }
  }

  public getIsMobileResolution(): boolean {
    this.setIsMobileResolution();
    return this.isMobileResolution;
  }
}
